namespace MaxVowels
{
	public class Substring
	{
		public int Count { get; set; }
		public string Value { get; set; }
	}
}