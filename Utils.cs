using System.Collections.Generic;

namespace MaxVowels
{
	public static class Utils
	{
		/// <summary>
		/// Cuenta las vocales en una palabra
		/// </summary>
		/// <param name="substringToEvaluate"></param>
		/// <returns></returns>
		public static int CountVowels(string word)
		{
			int countVowels = 0;

			foreach(char character in word)
			{
				if(Utils.IsVowel(character))
				{
					countVowels++;
				}
			}

			return countVowels;
		}

		/// <summary>
		/// Identifica si un caracter dado es vocal
		/// </summary>
		/// <param name="character">Caracter por validar</param>
		/// <returns>Verdadero si es vocal | Falso si no es vocal</returns>
		static bool IsVowel(char character)
		{
			// Hashset: 1) Valores únicos, 2) Se asume que las mismas vocales funcionan como keys
			HashSet<char> vowels = new HashSet<char>() { 'a', 'e', 'i', 'o', 'u' };

			bool isVowel = vowels.Contains(character);

			// Alternativa
			// IndexOf retorna -1 si no encuentra el caracter especificado. 
			// Será vocal si IndexOf es mayor o igual a cero
			// bool isVowel = "aeiouAEIOU".IndexOf(character) >= 0;

			return isVowel;
		}
	}
}