﻿using System;
using System.Collections.Generic;

namespace MaxVowels
{
	/// <summary>
	/// @migsalazar
	/// </summary>
	class Program
	{
		static void Main(string[] args)
		{
			string data = "asdasdsssaaaasdsdssssaa";

			int substringLength = 4;

			Substring substring = GetSubstringWithMoreVowels(data, substringLength);

			string output = $"COUNT: {substring.Count} / STRING: {substring.Value}";

			Console.WriteLine(output);

			// En windows:
			// Console.ReadKey();
		}

		/// <summary>
		/// Obtiene la cadena con la mayor cantidad de vocales, envuelta en un objeto Substring
		/// </summary>
		/// <param name="data">String de datos a evaluar</param>
		/// <param name="length">Longitud de bloques a evaliar</param>
		/// <returns>Objeto de tipo Substring (Substring es únicamente para fines de impresión)</returns>
		public static Substring GetSubstringWithMoreVowels(string data, int length)
		{
			Substring substring = new Substring();

			for(int i = 0; i <= (data.Length - length); i++)
			{
				string substringToEvaluate = data.Substring(i, length);

				int countVowels = Utils.CountVowels(substringToEvaluate);

				if(countVowels > substring.Count)
				{
					substring = new Substring {
						Count = countVowels,
						Value = substringToEvaluate,
					};
				}
			}

			return substring;
		}
	}
}
